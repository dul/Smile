
//import Input/Output library
#include <iostream>
#include <stdio.h>
//declare variable
char  smile[6]; //for the word that you want to 
int count; //for how many time you want to repeat

//this is the function;
int WriteOutput(int multiply){
	while(multiply>0){ //loop by multiply
		multiply--;
		//write the word
		int index=0;
		do{
			std::cout<<smile[index]; //write character
			index++; //increment
		}while(index<sizeof(smile)); //loop until the word all writen
	}
	std::cout<<"\n"; //write next line
	return count-1;
}

int main(){
	//initialize variable
	count=3;
	smile[0]='S';
	smile[1]='m';
	smile[2]='i';
	smile[3]='l';
	smile[4]='e';
	smile[5]='!';
	
	//start writing output
	write:
		count=WriteOutput(count);
		if(count>=0)
		goto write;
}


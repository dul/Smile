USE [Smile]
GO

DECLARE	@sqlCommand varchar(1000)
DECLARE @smile varchar (6)
DECLARE @count int
Declare @index int
Declare @index2 int
Set @index=0
Set @index2=0
SET @smile='Smile!'
SET @count=3

SET @sqlCommand = 'SELECT '
while(@index<@count)
BEGIN
	IF(@index!=0)
	SET @sqlCommand = Concat(@sqlCommand,'Union ALL select ')
	SET @index2=0
	WHILE(@index2<@count)
	BEGIN
		IF(@index2!=0)
		SET @sqlCommand = Concat(@sqlCommand,', ')
		IF(@count-@index2>@index)
		SET @sqlCommand = Concat(@sqlCommand,' ''Smile!'' as Just ')
		ELSE
		SET @sqlCommand = Concat(@sqlCommand,' '''' as Just ')
		
		SET @index2=@index2+1
	END

	SET @index=@index+1
END
EXECUTE(@sqlCommand)
GO
